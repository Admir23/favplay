class SongsController < ApplicationController
	helper_method :sort_column, :sort_direction

	before_action :authorize
	before_action :find_song, {only: [:show, :favorite]}

	def index
	 	@songs = Song.order("#{sort_column} #{sort_direction}").paginate(:page => params[:page], :per_page => 10)
  	@latest_albums = Album.order('created_at DESC').last(5)
	end
	
	 
  def show
	end

  def favorite
    @song = Song.find(params[:id])
    @song.update_favorites(current_user)
    @current_user = current_user

    respond_to do |format|
    	if @current_user.favorites.exists?	
	    	format.js { flash[:notice] = "#{@song.name} added to favorites!"}
	      format.json { render json: {message: message} }
	      format.html { redirect_to root_path }
      else
	      format.js { flash[:notice] = "#{@song.name} removed from favorites!"}
	      format.json { render json: {message: message} }
	      format.html { redirect_to root_path }
	    end
	  end  
  end

  private

  def find_song
  	@song = Song.find(params[:id])
  end

  def sortable_columns
  	["name", "length", "artist_id", "album_id", "genre_id"]
  end
  
  def sort_column
  	sortable_columns.include?(params[:column]) ? params[:column] : "name"
  end
  
  def sort_direction
  	%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"	
  end
end	 						