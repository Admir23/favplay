class GenresController < ApplicationController
	before_action :authorize
	before_action :find_genre, { only: [:show] }

	def index
		@genres = Genre.all
	end
	
	def show
		@albums = @genre.albums
		@songs = @genre.songs
	end

	private

	def find_genre
		@genre = Genre.find(params[:id])
	end	
end 		 				
