class HomeController < ApplicationController
	helper_method :sort_column, :sort_direction

	include SongHelper
	
	def index
		@songs = Song.search(params[:term]).order("#{sort_column} #{sort_direction}").paginate(:page => params[:page], :per_page => 10)
		@latest_albums = Album.order('created_at DESC').first(5)
		@top_songs = Song.order('favorites_count DESC').limit(5)
	end

	private

  def sortable_columns
  	["name", "length", "artist_id", "album_id", "genre_id"]
  end
  
  def sort_column
  	sortable_columns.include?(params[:column]) ? params[:column] : "name"
  end
  
  def sort_direction
  	%w[asc desc].include?(params[:direction]) ? params[:direction] : "asc"	
  end 						
end		