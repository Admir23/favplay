class ArtistsController < ApplicationController
	before_action :authorize

	def index
		@artists = Artist.order(created_at: :desc)
	end

	def show
		@artist = Artist.find(params[:id])
		@albums = @artist.albums
		
		# refactor with scope
		@top_songs = Song.where(artist_id: @artist.id).order('favorites_count DESC').limit(10)
  end
end  	