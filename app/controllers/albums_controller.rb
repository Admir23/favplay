class AlbumsController < ApplicationController
	before_action :authorize
	before_action :find_album, { only: [:show] }

	def index
		@albums = Album.order(created_at: :desc).paginate(:page => params[:page], :per_page => 4)
  end
	
	def show
     @songs = @album.songs
     @artist = @album.artist
	end


	private

	def find_album
		@album = Album.find(params[:id])
	end	
end		